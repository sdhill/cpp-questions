/*
 * Print a string in the following pattern. Given "prog"
 * p
 * pr
 * pro
 * prog
 * pro
 * pr
 * p
 */

#include <string>
#include <iostream>

void print_pattern(std::string str);

template<typename I>
void do_print(I start, I last, I end)
{
    std::string str(start, last);
    std::cout << str << '\n';
    if( last == end )
        return;
    do_print(start, ++last, end);
    std::cout << str << '\n';
}

void print_pattern(std::string str)
{
    if( str.empty() )
        return;
    do_print(str.begin(), str.begin(), str.end() );
}

int main()
{
    print_pattern("prog");
}
