#include <iostream>

int do_division_by_subtraction(int dividend, int divisor, int quotient);
int division_by_subtraction(int dividend, int divisor);

/*
 * Divide two strictly positive integers without remainder
 * and without using loops.
 *
 * For simplicity the function will return 0 if dividend < divisor or
 * if divisor is 0, or if either of dividend or divisor are negative.
 */
int do_division_by_subtraction(int dividend, int divisor, int quotient)
{
    if( dividend < divisor )
        return quotient;
    return do_division_by_subtraction( dividend - divisor, divisor, ++quotient);
}

int division_by_subtraction(int dividend, int divisor)
{
    if( dividend < 0 || divisor <= 0 )
        return 0;
    return do_division_by_subtraction(dividend, divisor, 0);
}

int main()
{
    std::cout << "7/3 = " << division_by_subtraction(7,3) << '\n';
    std::cout << "8/2 = " << division_by_subtraction(8,2) << '\n';
    std::cout << "1/2 = " << division_by_subtraction(1,2) << '\n';
    std::cout << "8/0 = " << division_by_subtraction(8,0) << '\n';
    std::cout << "-8/2 = " << division_by_subtraction(-8,2) << '\n';
    std::cout << "8/-2 = " << division_by_subtraction(8,-2) << '\n';
    std::cout << "0/2 = " << division_by_subtraction(0,2) << '\n';
    std::cout << "2/2 = " << division_by_subtraction(2,2) << '\n';
}
