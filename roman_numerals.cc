#include <string>
#include <iostream>
#include <vector>
#include <tuple>


std::string to_roman(int val);

/*
 * Convert an integer in the range [0, 4000] to a roman numeral.
 * If the number provided is invalid return an empty string.
 */
std::string to_roman(int val)
{
    if( 0 >= val || 4000 <= val )
        return std::string{};

    std::vector<std::tuple<int, const char*>> roman_numerals
      { {1000, "M"}, {900, "CM"}, {500, "D"}, {400, "CD"}, {100, "C"}, 
        {90, "XC"},  {50, "L"},   {40, "XL"}, {10, "X"},   {9, "IX"}, 
        {5, "V"},    {4, "IV"},   {1, "I"} };

    std::string result;
    for( const auto &r : roman_numerals )
    {
        int r_val;
        const char* r_str;
        std::tie(r_val, r_str) = r;
        while( val >= r_val )
        {
            val -= r_val;
            result.append(r_str);
        }
    }

    return result;
}

int main()
{
    std::cout << "1: " << to_roman(1) << '\n';
    std::cout << "2: " << to_roman(2) << '\n';
    std::cout << "4: " << to_roman(4) << '\n';
    std::cout << "5: " << to_roman(5) << '\n';
    std::cout << "9: " << to_roman(9) << '\n';
    std::cout << "10: " << to_roman(10) << '\n';
    std::cout << "3300: " << to_roman(3300) << '\n';
}
