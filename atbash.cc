
#include <cctype>
#include <iostream>
#include <algorithm>

std::string atbash(const std::string&);
void atbash2(std::string&);
void atbash3(std::string&);

/*
 * Apply the atbash cipher to str while removing all non-alpha
 * characters and changing to upper case.
 */
std::string atbash(const std::string &str)
{
    std::string result;
    result.reserve(str.length() + 1);
    for( const char ch : str )
        if( std::isalpha(ch) ) 
            result.push_back( static_cast<char>((25 - (std::toupper(ch) - 'A')) + 'A' ));
    return result;
}

/*
 * Apply the atbash cipher to str inplace and ignore any non-alpha characters.
 */
void atbash2(std::string &str)
{
    for( char &ch : str )
        if( std::isalpha(ch) ) 
            ch =  static_cast<char>((25 - (std::toupper(ch) - 'A')) + 'A' );
}

/*
 * Apply the atbash cipher to str inplace using the STL and assuming that
 * str only contains alpha characters.
 */
void atbash3(std::string &str)
{
    auto fun = [](char &ch) {
        ch = static_cast<char>((25 - (std::toupper(ch) - 'A')) + 'A' );
    };

    std::for_each(str.begin(), str.end(), fun);
}

int main()
{
    std::string str("abcdefghijklmnopqrstuvwxyz");
    std::cout << "original: " << str << '\n';
    std::cout << "  atbash: " << atbash(str) << "\n\n";

    str = "ZYXWVUTSRQPONMLKJIHGFEDCBA";
    std::cout << "original: " << str << '\n';
    std::cout << "  atbash: " << atbash(str) << "\n\n";

    str = "hello there   1234 %^& this is a test string!!";
    std::cout << "original: " << str << '\n';
    std::cout << "  atbash: " << atbash(str) << "\n\n";
   
    std::cout << "atbash 2\n"; 
    str = "abcdefghijklmnopqrstuvwxyz";
    std::cout << "original: " << str << '\n';
    atbash2(str);
    std::cout << " atbash2: " << str << "\n\n";

    str = "ZYXWVUTSRQPONMLKJIHGFEDCBA";
    std::cout << "original: " << str << '\n';
    atbash2(str);
    std::cout << " atbash2: " << str << "\n\n";

    str = "hello there   1234 %^& this is a test string!!";
    std::cout << "original: " << str << '\n';
    atbash2(str);
    std::cout << " atbash2: " << str << "\n\n";
   
    std::cout << "atbash 3\n"; 
    str = "abcdefghijklmnopqrstuvwxyz";
    std::cout << "original: " << str << '\n';
    atbash3(str);
    std::cout << " atbash3: " << str << "\n\n";

    str = "ZYXWVUTSRQPONMLKJIHGFEDCBA";
    std::cout << "original: " << str << '\n';
    atbash3(str);
    std::cout << " atbash3: " << str << "\n\n";
}

