#include <algorithm>
#include <iostream>
#include <vector>
#include <numeric>

// The question was given a vector of values how to use a simple if
// statement to test if they all meet conditions. This can be done
// with the algorithms library using all_of or none_of. I also 
// made accumulate work as well.
//
int main()
{
    std::vector<int> v1{2,3,5};

    std::cout << "all_of\n";
    auto fun1 = [](const int val){ return 0 < val; };
    if( std::all_of(v1.begin(), v1.end(), fun1) )
        std::cout << "all greater than 0\n";
    else
        std::cout << "some not greater than 0\n";

    std::cout << "none_of\n";
    auto fun2 = [](const int val){ return val < 1; };
    if( std::none_of(v1.begin(), v1.end(), fun2) )
        std::cout << "all greater than 0\n";
    else
        std::cout << "some not greater than 0\n";
    
    std::cout << "accumulate\n";
    auto fun3 = [](bool b, const int val){ return ( b )?0 < val : b; };
    if( std::accumulate(v1.begin(), v1.end(), true, fun3) )
        std::cout << "all greater than 0\n";
    else
        std::cout << "some not greater than 0\n";
}
