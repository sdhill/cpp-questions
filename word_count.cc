#include <iostream>
#include <tuple>
#include <string>
#include <cctype>
#include <numeric>

//
// Count the number of alpha characters and words in a string. A word being
// any string of alpha characters between any non-alpha characters.
//
std::tuple<int,int> sentence_count(const std::string &sentence)
{
    int num_chars = 0;
    int num_words = 0;
    bool in_word = false;

    for( char ch : sentence )
    {
        if( std::isalpha(ch) )
        {
            ++num_chars;
            if( !in_word )
            {
                in_word = true;
                ++num_words;
            }
        }
        else
            in_word = false;
    }

    return {num_chars, num_words};
}

int count_chars(const std::string &sentence)
{
    auto fun = [](int acc, char ch){ 
        return ( std::isalpha(ch) )? ++acc : acc; };
    return std::accumulate(sentence.begin(), sentence.end(), 0, fun);
}


int main()
{
    std::string s{"hello world you suck"};
    //auto [num_chars, num_words] = sentence_count(s); // not on mac
    int num_chars = 0;
    int num_words = 0;
    std::tie(num_chars, num_words) = sentence_count(s);
    std::cout << s << " has " << num_chars << " characters and " << num_words << " words\n";

    std::cout << "accumulate: " << count_chars(s) << '\n';

}

