#include <string>
#include <iostream>
#include <algorithm>
#include <iterator>

// Write a program that will print a string forwards and then backwards.
//

// Write all elements in the range (begin, end] to out.
// This will work with forward and reverse iterators.
//
template<typename I>
void iter_print(std::ostream &out, I begin, I end)
{
    for( ; begin != end; ++begin )
        out << *begin;
    out << '\n';
}

// The same as iter_print but using std::copy and ostream iterator.
//
template<typename I>
void print_with_copy(std::ostream &out, I begin, I end)
{
    std::copy(begin, end, std::ostream_iterator<char>(out));
    out << '\n';
}

int main()
{
    std::string str("this is a multi-word string");

    std::cout << "forwards: ";
    iter_print(std::cout, str.begin(), str.end());
    std::cout << "reverse: ";
    iter_print(std::cout, str.rbegin(), str.rend());
    std::cout << "forwards with std::copy: ";
    print_with_copy(std::cout, str.begin(), str.end());
    std::cout << "reverse with std::copy: ";
    print_with_copy(std::cout, str.rbegin(), str.rend());
}
