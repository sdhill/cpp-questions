#include <string>
#include <algorithm>
#include <iostream>

// Write a function to reduce multiple spaces between two words 
// to a single space.
void remove_spaces(std::string &str)
{
    auto iter = str.begin();
    while( iter < str.end() )
    {
        iter = std::adjacent_find(iter, str.end(), 
                [](char ch1, char ch2){ 
                return std::isspace(ch1) && std::isspace(ch2); } );
        if( iter < str.end() )
            iter = str.erase(iter);
    }
}

int main()
{
    std::string s{"hello  there  world"};
    std::cout << s << '\n';
    remove_spaces(s);
    std::cout << s << '\n';
    s = "        ";
    std::cout << '\'' << s << "\'\n";
    remove_spaces(s);
    std::cout << '\'' << s << "\'\n";
}
