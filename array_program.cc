/*
Program. Write a program that creates an integer constant called SIZE and initialize to the size of the array you will be creating. Use this constant throughout your functions you will implement for the next parts and your main program. Also, in your main program create an integer array called numbers and initialize it with the following values (Please see demo program below):

68, 100, 43, 58, 76, 72, 46, 55, 92, 94, 73, 68, 85, 48, 47, 62, 56, 75, 60, 60, 59, 77, 40, 72, 73, 95, 90, 89, 73, 93

i) void printArray(const int [] numbers). This function accepts an array as its first argument and prints each value in the array.

ii) int getSum(const int [] numbers). This function accepts an array as its argument and returns the sum of all the values in the array.

iii) double getAverage(const int [] numbers). This function accepts an array as its argument and returns the average of all the values in the array.

iv) double getMedian(const int [] numbers). This function accepts an array and returns a double which is the median (middle element) or average of the 2 middle values. (Hint: sort the array to find the median element). Also, use pointer notation * instead of array [] notation in this function.

v) int getMinValue(const int [] numbers). This function accepts an array as its argument and returns the minimum value in the array.

vi) int getMaxValue(const int [] numbers). This function accepts an array as its argument and returns the maximum value in the array.

vii) void copyArray(int [] dstnumbers, const int [] srcnumbers). This function accepts 2 arrays arguments. This function copy the value of the elements from srcnumbers array to dstnumbers array.

viii) void sortIncrease(int [] numbers). This function accepts an array as its arguments and sorts the array in increasing order.

ix) void sortDecrease(int [] numbers). This function accepts an array as it’s arguments and sorts the array in decreasing order.

x) int findNumber(const int [] numbers, int number). This function returns the index location of the number found in the array. Otherwise, it returns -1.

xi) void printNumbersLessThan(const int [] numbers, int number). This function prints the numbers of in the array that are less than the number passed in to the function.

xii) void printNumberBiggerThan(const int [] numbers, int number). This function prints the numbers in the array that is greater than the number pass in to the function.

For this we will use C arrays but will also use the STL.
*/

#include <iostream>
#include <iterator>
#include <numeric>

void printArray(const int numbers[]);
int getSum(const int numbers[]);
double getAverage(const int numbers[]);
double getMedian(const int numbers[]);
int getMinValue(const int numbers[]);
int getMaxValue(const int numbers[]);
void copyArray(int dstnumbers[], const int srcnumbers[]);
void sortIncrease(int numbers[]);
void sortDecrease(int numbers[]);
int findNumber(const int numbers[], int number);
void printNumbersLessThan(const int numbers[], int number);
void printNumbersBiggerThan(const int numbers[], int number);
void testSorting(const int numbers[]);

const int SIZE = 30;

int main()
{
    int numbers[SIZE]{ 
        68, 100, 43, 58, 76, 72, 46, 55, 92, 94, 73, 68, 85, 48, 47, 
        62, 56, 75, 60, 60, 59, 77, 40, 72, 73, 95, 90, 89, 73, 93 };

    std::cout << "All numbers\n";
    printArray(numbers);
    std::cout << "sum: " << getSum(numbers) << '\n';
    std::cout << "average: " << getAverage(numbers) << '\n';
    std::cout << "median: " << getMedian(numbers) << '\n';
    std::cout << "min value: " << getMinValue(numbers) << '\n';
    std::cout << "max value: " << getMaxValue(numbers) << '\n';
    std::cout << "numbers less than 72\n";
    printNumbersLessThan(numbers, 72);
    std::cout << "numbers bigger than 72\n";
    printNumbersBiggerThan(numbers, 72);
    int idx = findNumber(numbers, 68);
    std::cout << "find 68: ";
    if( -1 == idx )
        std::cout << "was not found\n";
    else
        std::cout << "found at index " << idx << '\n';
    
    idx = findNumber(numbers, 93);
    std::cout << "find 93: ";
    if( -1 == idx )
        std::cout << "was not found\n";
    else
        std::cout << "found at index " << idx << '\n';
    
    idx = findNumber(numbers, 25);
    std::cout << "find 25: ";
    if( -1 == idx )
        std::cout << "was not found\n";
    else
        std::cout << "found at index " << idx << '\n';
    testSorting(numbers);
}

void printArray(const int numbers[])
{
    std::copy(numbers, numbers + SIZE - 1, 
            std::ostream_iterator<int>(std::cout, ", " ));
    std::cout << numbers[SIZE - 1] << '\n';
}

int getSum(const int numbers[])
{
    return std::accumulate(numbers, numbers + SIZE, 0);
}

double getAverage(const int numbers[])
{
    return static_cast<double>(getSum(numbers)) / static_cast<double>(SIZE);
}

/*
 * return the middle value or the average of the two middle values
 * if SIZE is even.
 */
double getMedian(const int numbers[])
{
    int temp[SIZE];
    copyArray(temp, numbers);
    sortIncrease(temp);
    
    size_t idx = SIZE / 2;
    double median = temp[idx];
    if( 0 == SIZE % 2 )
        median = (median + temp[idx-1]) / 2.0;

    return median;
}

int getMinValue(const int numbers[])
{
    return *(std::min_element(numbers, numbers + SIZE));
}

int getMaxValue(const int numbers[])
{
    return *(std::max_element(numbers, numbers + SIZE));
}

void copyArray(int dstnumbers[], const int srcnumbers[])
{
    std::memcpy(dstnumbers, srcnumbers, sizeof(int) * SIZE);
}

void sortIncrease(int numbers[])
{
    std::sort(numbers, numbers + SIZE);
}

void sortDecrease(int numbers[])
{
    std::sort(numbers, numbers + SIZE, std::greater<int>());
}

/* return the first index of number or -1 if not found */
int findNumber(const int numbers[], int number)
{
    int idx = -1;
    const int *end = numbers + SIZE;
    const int *item = std::find(numbers, end, number);
    if( item != end )
        idx = static_cast<int>(item - numbers);
    return idx;
}

void printNumbersLessThan(const int numbers[], int number)
{
    auto fun = [&number](const int val){ return val < number; };

    std::copy_if(numbers, numbers + SIZE - 1, 
            std::ostream_iterator<int>(std::cout, ", " ), fun);
    std::cout << "\b\b \n";  // erase the last comma
}

void printNumbersBiggerThan(const int numbers[], int number)
{
    auto fun = [&number](const int val){ return val > number; };

    std::copy_if(numbers, numbers + SIZE - 1, 
            std::ostream_iterator<int>(std::cout, ", " ), fun);
    std::cout << "\b\b \n";
}

void testSorting(const int numbers[])
{
    int temp[SIZE];
    copyArray(temp, numbers);

    std::cout << "original array:\n";
    printArray(temp);

    std::cout << "sorted ascending:\n";
    sortIncrease(temp);
    printArray(temp);

    std::cout << "sorted descending:\n";
    sortDecrease(temp);
    printArray(temp);
    
}


