#include <iostream>
#include <string>
#include <numeric>
using namespace std;

//
// Write a program to sum the ASCII values of a string. This shows 3 ways
// that it may be done. A range based for loop, std::accumulate, and 
// std::reduce.
//
int main()
{
    int sum = 0;
    string text = "0123";
    cout << "loop\n";
    for( const char ch : text )
    {
        sum += ch;
        cout << (int)ch << '\n';
    }
    cout << "sum: " << sum << '\n';
    
    cout << "accumulate\n";
    sum = 0;
    cout << "sum: " << accumulate(text.begin(), text.end(), 0) << '\n';

    cout << "reduce\n";
    sum = 0;
    cout << "sum: " << reduce(text.begin(), text.end(), 0) << '\n';

}

